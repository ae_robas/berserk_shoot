﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossmanager : MonoBehaviour
{
    public GameObject spawnee;
    public bool stopSpawning = false;
    public float spawnTime;
    public float spawnDelay;
    public GUIStyle progress_empty;
    public GUIStyle progress_full;

    //current progress
    public float barDisplay;


    private void Start()
    {
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
   
      


    }


    public void SpawnObject()
    {
        Instantiate(spawnee, new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
       
        if (stopSpawning)
        {
            CancelInvoke("SpawnObject");
        }
    }
}
