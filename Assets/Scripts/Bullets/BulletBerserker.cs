﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBerserker : MonoBehaviour
{
    public float speed;

    public int health;

    int damage;


    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime, 0, 0);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish")
        {
            health -= damage;

            if (health <= 0)
            {
                Destroy(gameObject);
            }

        }           
        
        
          else if (other.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }

}
