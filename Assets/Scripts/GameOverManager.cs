﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    [HideInInspector] public bool isPaused = false;
    [SerializeField] GameObject canvas;
    [SerializeField] PlayerBehaviour player;
    [SerializeField] Text score;

    [SerializeField] ScoreManager sm;

    public void PlayAgain(){
        isPaused = false;
        Time.timeScale = 1.0f;
        gameObject.GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("Game");
    }

    public void Quit(){
        Time.timeScale = 1.0f;
        gameObject.GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("MainMenu");
    }


    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (player.lives <= 0)
        {
            ActivateGameOver();
        }   
    }

    void ActivateGameOver(){
        isPaused = true;

        score.text = "SCORE: " + sm.scoreInt.ToString();

        canvas.SetActive(true);

        Time.timeScale = 0;
    }
}
