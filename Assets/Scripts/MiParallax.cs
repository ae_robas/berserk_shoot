﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiParallax : MonoBehaviour
{
    public float posInicial,posFinal,velocidad;

    
    // Update is called once per frame
    void Update()
    {
        transform.Translate(velocidad*Time.deltaTime, 0, 0);

        if (transform.position.x < posFinal)
        {
            transform.Translate(posInicial-posFinal,0,0);
        }
    }
}
