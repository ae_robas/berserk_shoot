﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay(){
        Debug.LogError("He pulsado Play");

        SceneManager.LoadScene("Game");
    }
    public void PulsaOptions(){
        Debug.LogError("He pulsado Options");       

        SceneManager.LoadScene("Options");
    }

    public void PulsaCredits()
    {
        Debug.LogError("He pulsado Credits");

        SceneManager.LoadScene("Credits");
    }


    public void PulsaExit(){
        Application.Quit();
    }
    public void PulsaBacktoMenu()
    {
        Debug.LogError("He pulsado PulsaBacktoMenu");

        SceneManager.LoadScene("MainMenu");
    }


}
